const details = `key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392`
const myCardId = '5c99cec05d51624fdc4de1d5'
let checklistItems = [];
let checkLists = [];
const check = document.getElementById('create');
const container = document.querySelector('.container');
check.addEventListener('submit', createChecklist);

const init = async () => {
  try {
    let response = await fetch(`https://api.trello.com/1/cards/${myCardId}/checklists?fields=name&${details}`, { method: 'GET' })
    let checkLists = await response.json();
    if (checkLists.length !== 0) {
      checkLists.forEach((element) => {
        displayCheckList(element)
        if (element.checkItems.length !== 0) {
          checklistItems = checklistItems.concat(element.checkItems);
          element.checkItems.forEach((item) => display(item))
        }
      });
    }
  } catch (err) {
    alert(err);
  }
}

const checklistEntities = ((e) => {
  if (e.target.classList.contains('deleteChecklist')) {
    e.preventDefault();
    fetch(`https://api.trello.com/1/checklists/${e.target.parentElement.parentElement.id}?${details}`, { method: 'DELETE' })
      .then(
        container.removeChild(e.target.parentElement.parentElement)
      ).catch(function (error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
      });
  }
  else if (e.target.classList.contains('submit')) {
    e.preventDefault();
    let newItem = document.getElementById('item' + e.target.parentElement.parentElement.id);
    if (newItem.value) {
      fetch(`https://api.trello.com/1/checklists/${e.target.parentElement.parentElement.id}/checkItems?name=${newItem.value}&pos=bottom&${details}`, { method: 'POST' })
        .then(data => data.json())
        .then(data => {
          display(data);
          checklistItems.push(data);
        }).catch(function (error) {
          console.log('There has been a problem with your fetch operation: ', error.message);
        });
    }
    newItem.value = '';
  }
})


const changeItemStatus = ((e) => {
  if (e.target.classList.contains('delete')) deleteitems(e);
  else if (e.target.classList.contains('check')) {
    let value = 'incomplete';
    checklistItems.forEach((item) => {
      if (item.id === e.target.parentElement.id)
        if (item.state === 'incomplete') {
          item.state = 'complete';
          value = 'complete';
        }
        else {
          item.state = 'incomplete';
        }
    });
    fetch(`https://api.trello.com/1/cards/${myCardId}/checkItem/${e.target.parentElement.id}?state=${value}&${details}`, { method: 'PUT' })
      .catch(function (error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
      });
  }
  else if (e.target.classList.contains('itemName')) {
    let Enter = document.getElementById(e.target.id)
    Enter.addEventListener('keypress', save);
  }
})
const deleteitems = (e) => {
  fetch(`https://api.trello.com/1/checklists/${e.target.parentElement.parentElement.id}/checkItems/${e.target.parentElement.id}?${details}`, { method: 'DELETE' })
    .then(res => {
      checklistItems = checklistItems.filter((item) => item.id !== e.target.parentElement.id);
      e.target.parentElement.parentElement.removeChild(e.target.parentElement);
    }).catch(function (error) {
      console.log('There has been a problem with your fetch operation: ', error.message);
    });
}

const save = ((e) => {
  if (e.key == 'Enter') {
    fetch(`https://api.trello.com/1/cards/${myCardId}/checkItem/${e.target.parentElement.id}?name=${e.target.value}&${details}`, { method: 'PUT' })
      .then(checklistItems.forEach((item) => {
        if (item.id === e.target.parentElement.id) item.name = e.target.value;
      }))
      .catch(function (error) {
        console.log('There has been a problem with your fetch operation: ', error.message);
      });
  }
})

function createChecklist(e) {
  e.preventDefault();
  let name = document.getElementById("checklistName");
  fetch(`https://api.trello.com/1/checklists?idCard=${myCardId}&name=${name.value}&${details}`, { method: 'POST' })
    .then((data) => data.json())
    .then(data => {
      checkLists.push(data);
      displayCheckList(data);
    })
    .catch(function (error) {
      console.log('There has been a problem with your fetch operation: ', error.message);
    });
  name.value = '';
}

const displayCheckList = ((element) => {

  let template = `<div id="${element.id}" >      
    <h3>${element.name}<button class="deleteChecklist">X</button></h3>
    <form id="addform${element.id}">
    <input type ="text" id="item${element.id}" required>
    <input type ="submit" class="submit">
    </form>
    <h4>Items</h4>
  </div>`
  let e = document.createElement('div');
  e.innerHTML = template;
  container.appendChild(e.firstChild);
  let item = document.getElementById(element.id);
  item.addEventListener('click', checklistEntities);
})

const display = ((element) => {
  let status = "unchecked"
  if (element.state === 'complete') status = "checked";
  let template = `<div id="${element.id}" class="d-flex">
        <input type ="checkbox" class="check " ${status}>
        <input type="text" id="text${element.id}" class="itemName" value="${element.name}">
        <button class="delete">X</button>
    </div>`
  let e = document.createElement('div');
  e.innerHTML = template;
  div = document.getElementById(element.idChecklist);
  div.appendChild(e.firstChild);
  let alterItems = document.getElementById(element.id);
  alterItems.addEventListener('click', changeItemStatus);
})

init();