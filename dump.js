myKey = 'aa091b9b6bf7f90452318b602213ff02'
myToken = '3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392'
BoardUrl = '4I0OTsZX'
myListId = '5c99cb9092b717629acff3f0'
myCardId = '5c99cec05d51624fdc4de1d5'
myChecklistId = '5c99cffdfebec14f576453ae'
var checklistItems = [];


let allBoards = 'https://api.trello.com/1/members/me/boards?fields=name&key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392'
let myBoard = 'https://api.trello.com/1/boards/4I0OTsZX?fields=name&key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392'
let allLists = 'https://api.trello.com/1/boards/4I0OTsZX/lists?fields=name&key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392'
let myCards = 'https://api.trello.com/1/lists/5c99cb9092b717629acff3f0/cards?fields=name&key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392'
let allCheckLists = 'https://api.trello.com/1/cards/5c99cec05d51624fdc4de1d5/checklists?fields=name&key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392'
let myChecklist = 'https://api.trello.com/1/checklists/5c99cffdfebec14f576453ae/checkItems?key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392'

const fetchUrl = ((url) =>
    new Promise((resolve, reject) => {
        fetch(url).then((board) => {
            return board.json();
        }).then((boardObj) => {
            resolve(boardObj);
        })
    }))

const access = async () => {
    let boards = await fetchUrl(allBoards);
    let boardId = await fetchUrl(myBoard);
    let lists = await fetchUrl(allLists);
    let list = await fetchUrl(myCards);
    let checkLists = await fetchUrl(allCheckLists);
    checklistItems = await fetchUrl(myChecklist)
    // console.log(boards);
    // console.log(boardId)
    // console.log(lists);
    // console.log(list)
    console.log(checkLists)
    // console.log(checklistItems)
    if (checklistItems.length > 0) {
        checklistItems.forEach(element => {
            display(element);
        })
    }
}
access();

const display = (element) => {
    var item = element.name;
    var li = document.createElement('li');
    li.className = 'list-group-item';
    li.id = element.id;
    var checkBox = document.createElement('INPUT');
    checkBox.setAttribute('type', 'checkbox')
    if (element.state === 'complete') checkBox.checked = true;
    checkBox.className = 'm-2 check';
    li.appendChild(checkBox)
    let text = document.createElement('INPUT')
    text.setAttribute('type', 'text');
    text.value = item;
    text.className = 'border-0  w-75'
    li.appendChild(text);
    var deleteBtn = document.createElement('button');
    deleteBtn.className = 'btn btn-danger btn float-right delete';
    li.appendChild(deleteBtn);
    itemList.appendChild(li);
}

var form = document.getElementById('addForm');
var itemList = document.getElementById('items');
var filter = document.getElementById('filter');


form.addEventListener('submit', addItem);
itemList.addEventListener('click', changeItem);

// Add item
function addItem(e) {
    e.preventDefault();
    var newItem = document.getElementById('item').value;
    var request = new XMLHttpRequest()
    request.open('POST', "https://api.trello.com/1/checklists/5c99cffdfebec14f576453ae/checkItems?name=" + newItem + "&pos=bottom&key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392", true)
    request.onload = function () {
        display(JSON.parse(this.response));
        checklistItems.push(JSON.parse(this.response));
    }
    request.send()
    document.getElementById('item').value = '';
    console.log(checklistItems)
}

// Remove item
function changeItem(e) {
    if (e.target.classList.contains('delete')) {
        if (confirm('Are You Sure?')) {
            var request = new XMLHttpRequest()
            request.open('DELETE', "https://api.trello.com/1/checklists/5c99cffdfebec14f576453ae/checkItems/" + e.target.parentElement.id + "?key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392", true)
            request.send();
            checklistItems = checklistItems.filter((item) => item.id !== e.target.parentElement.id);
            itemList.removeChild(e.target.parentElement);
        }
    }
    else if (e.target.classList.contains('check')) {
        var request = new XMLHttpRequest()
        var value = 'incomplete';
        checklistItems.forEach((item) => {
            if (item.id === e.target.parentElement.id)
                if (item.state === 'incomplete') {
                    item.state = 'complete';
                    value = 'complete';
                }
                else {
                    item.state = 'incomplete';
                }
        }); console.log(checklistItems)
        console.log(e.target.parentElement.id)
        request.open('PUT', "https://api.trello.com/1/cards/5c99cec05d51624fdc4de1d5/checkItem/" + e.target.parentElement.id + "?state=" + value + "&key=aa091b9b6bf7f90452318b602213ff02&token=3ea97de0769d5a428fc4e1d5bb38592bcbda8b155c142b2e1697b035365d9392", true)
        request.send();
    }
    // console.log(checklistItems)
    else {
        // e.target.value=e.target.value;
        // console.log(e.target.value)
    }
}
// console.log(checklistItems)

